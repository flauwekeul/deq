# DEQ README #

## Introduction ##

DEQ - Digital Event Queue - is a javascript prototype/object that is designed to be used to act as a queue for datalayers
in event-driven datalayer implementations.
It allows you to create multiple queues where you can push events to and where you can register listeners that will be
notified when an event has occurred.

*Commands*, e.g. adding an event to the queue or registering a listerner for an event, can be pushed to the queue as if
they are pushed to an array. This allows asynchronously adding events and registering listeners.
More information about these *commands* can be found in the [commands documentation](./doc/DEQ-commands.md) which is to be found in the [/doc](./doc) folder.

## Contributors ##

The current version of DEQ is the result of efforts from the following contributors.

* Koen Crommentuijn <koen.crommentuijn@adversitement.com>
* Martijn Schoenmakers <martijn.schoenmakers@adversitement.com>

## License
Adversitement Digital Event Queue.
Copyright (c) 2016-2017: Insite Innovations and Properties B.V.,
Liessentstraat 9A, 5405 AH Uden, The Netherlands.
Email: [legal@adversitement.com](mailto:legal@adversitement.com). All rights reserved.

This software is licensed under terms which can be found at:
[https://bitbucket.org/adversitement/deq/src/master/license.txt](./license.txt)
These terms include the terms of the Lesser General Public License (LGPL),
version 3, as published by the Free Software Foundation. The full LGPL license
text can be found at: http://www.gnu.org/copyleft/lesser.html
