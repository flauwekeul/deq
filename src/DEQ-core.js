/**
 @preserve Adversitement Digital Event Queue.
 Copyright (c) 2016-2017: Insite Innovations and Properties B.V.,
 Liessentstraat 9A, 5405 AH Uden, The Netherlands.
 Email: legal@adversitement.com. All rights reserved.

 This software is licensed under terms which can be found at:
 https://bitbucket.org/adversitement/deq/src/master/license.txt
 These terms include the terms of the Lesser General Public License (LGPL),
 version 3, as published by the Free Software Foundation. The full LGPL license
 text can be found at: http://www.gnu.org/copyleft/lesser.html.
 **/


/** DEQ Lib v1.0.0 core **/
/***
 * DEQ factory is a singleton factory that has two methods, being DEQFactory.get and DEQFactory.getAll
 * See the description below (in the public interface section) for more information regarding those methods.
 ***/
var DEQFactory = (typeof(DEQFactory) == 'object') ? DEQFactory : (function(){
    DigitalEventQueue.prototype = [];                               // inherit from Array
    DigitalEventQueue.prototype.constructor = DigitalEventQueue;    // point constructor to DigitalEventQueue instead of Array
    function DigitalEventQueue(name, previous_commands) {
        var DEQ = this;
        DEQ.version = "1.0.0 core";
        DEQ.listeners = [];
        DEQ.events = [];

        DEQ.__private = {};
        DEQ.__private.lib = {

            // isArray: tests if parameter is an array and returns true if param is array
            isArray: function(o) {
                return o && Object.prototype.toString.call(o) === '[object Array]';
            },

            // copyObj: returns a copy of an object that does not reference to the object copied from. Circular references in objects are not allowed.
            copyObj: function(obj) {
                return JSON.parse(JSON.stringify(obj || {}));
            },

            // logError: adds a descriptive error event to the event queue
            logError: function(errorType, errorMsg, errorObj) {
                var eventObj = {
                    deq_error_type: errorType,
                    deq_error_message: errorMsg
                };
                if (typeof(errorObj) == "object") {
                    if (errorObj.fileName && errorObj.lineNumber)   eventObj.deq_error_location = errorObj.fileName + " (" + errorObj.lineNumber + ")";
                    if (errorObj.name && errorObj.message)          eventObj.deq_error_details = errorObj.name + ": " + errorObj.message;
                    if (errorObj.stack)                             eventObj.deq_error_stack = errorObj.stack.toString();
                    if (errorObj.deq_listener)                      eventObj.deq_error_listener = errorObj.deq_listener;
                    if (errorObj.deq_event)                         eventObj.deq_error_event = errorObj.deq_event;
                }
                DEQ.push(["add event", "deq error", eventObj]);
            },

            // callEventCallback: publish an event to a specific listener
            callEventCallback: function(event_name, event_data, listener_callback, listener_name) {
                try {
                    var event_data_copy = DEQ.__private.lib.copyObj(event_data);
                    listener_callback.call(listener_callback, event_name, event_data_copy);
                } catch (e) {
                    if (event_name != "deq error") {
                        e.deq_listener = listener_name;
                        e.deq_event = event_name;
                        DEQ.__private.lib.logError("listener", "listener (" + listener_name + ") failed on '" + event_name, e);
                    }
                }
            },

            // addListener: register a listener and, if needed, publish all former events to this listener
            addListener: function(listener_name, callback, event_name, include_history) {
                // Test input
                if ((typeof(listener_name) == "undefined" || typeof(listener_name) == "string" ) &&
                    typeof(event_name) == "string" &&
                    typeof(callback) == "function") {

                    listener_name = listener_name || "unnamed listener";

                    // Add event listener
                    DEQ.listeners.push([listener_name, event_name, callback]);

                    // If listener should also trigger for events that already occurred (true by default)
                    if (typeof(include_history) == "undefined" || include_history) {

                        // Setup regular expression to match on event_name
                        var event_name_re = new RegExp('^' + event_name + '$', 'i');    // Note: case insensitive

                        // Run through event history
                        for (var i = 0; i < DEQ.events.length; i++) {

                            // If event present, trigger callback function
                            if (typeof(DEQ.events[i][0]) == 'string' && DEQ.events[i][0].match(event_name_re)) {
                                DEQ.__private.lib.callEventCallback(DEQ.events[i][0], DEQ.events[i][1], callback, listener_name);
                            }
                        }
                    }
                }
                else {
                    DEQ.__private.lib.logError("add listener", "invalid input", {deq_listener: listener_name});
                }
            },

            // addEvent: add an event to the queue
            addEvent: function(event_name, event_data) {
                if (typeof(event_name) == "string" && (typeof(event_data) == "undefined" || typeof(event_data) == "object")) {
                    var event_data_copy;
                    // copy event object in event_data, if copy action fails report error and drop event
                    try {
                        event_data_copy = DEQ.__private.lib.copyObj(event_data);
                    }
                    catch (e) {
                        e.deq_event = event_name;
                        DEQ.__private.lib.logError("add event", "invalid event object", e);
                        return false;
                    }

                    // add event name to event data object
                    event_data_copy.event = event_name;
                    // add browser timestamp to event data object
                    event_data_copy.event_timestamp_deq = (new Date().getTime());

                    // store a copy of the event in the event history
                    DEQ.events.push([event_name, event_data_copy]);

                    // Run through event listeners
                    for (var i = 0, len = DEQ.listeners.length; i < len; i++) {
                        // If event present, trigger callback function
                        var event_name_re = new RegExp('^' + DEQ.listeners[i][1] + '$', 'i');   // Note: case insensitive
                        if (event_name.match(event_name_re)) {
                            DEQ.__private.lib.callEventCallback(event_name, event_data_copy, DEQ.listeners[i][2], DEQ.listeners[i][0]);
                        }
                    }
                }
                else {
                    var errorObj = {};
                    if (typeof(event_name) == "string") errorObj.deq_event = event_name;
                    DEQ.__private.lib.logError("add event", "invalid input", errorObj);
                }
            }
        };

        // Map commands to private functions
        DEQ.__private.commandMapping = {
            "add event": DEQ.__private.lib.addEvent,
            "add listener": DEQ.__private.lib.addListener
        };
        // Override push functionality to execute the correct command
        DEQ.push = function(commandArr) {
            if (DEQ.__private.lib.isArray(commandArr) && typeof(commandArr[0]) == "string") {
                var command_copy = commandArr.slice(0),
                    command = command_copy.shift();

                if (DEQ.__private.commandMapping.hasOwnProperty(command)) {
                    DEQ.__private.commandMapping[command].apply(this, command_copy);
                } else {
                    DEQ.__private.lib.logError("push", "command '" + command + "' is unknown");
                }
            }
            else {
                DEQ.__private.lib.logError("push", "invalid input: use .push(['#command#',#param1#,#param2#,...])");
            }
        };

        /**
         * Pushes a bulk of commands at once.
         * Be carefull with this function, as:
         *  * it changes the input array in-place, such that the list is empty afterwards;
         *  * the order in which commands are pushed from different "temporary queues" may matter e.g. when listeners have the include_history option set to false!
         *
         *  In most casus should only be used internally for initialization.
         *
         * @param {Array} commands should be an Array consisting of "commands". After running the function the array will be empty.
         */
        DEQ.pushBulk = function(commands) {
            // Process events from bulk
            if (DEQ.__private.lib.isArray(commands)) {
                while (commands.length > 0) {
                    DEQ.push(commands.shift());
                }
            }
        };

        // Init
        DEQ.name = name;
        if (Object.getPrototypeOf(DEQ).constructor === DigitalEventQueue) {
            // Process commands that were added in the arrayRef already
            DEQ.pushBulk(previous_commands);
        }
    }

    /*** Start factory code ***/
    var publicInterface = {};
    var _queueList = {};

    /**
     * Return a queue identified by name name.
     * If this queue does not yet exists, one is created on the fly.
     *
     * @param name The name to identify the queue. This name can be used to get the same queue from multiple locations.
     * @param {Array} [arrayRef] an option array that might already contain commands that needs to be added to the queue already
     * @returns {DigitalEventQueue}
     */
    publicInterface.get = function(name, arrayRef){
        if (!_queueList.hasOwnProperty(name)) {
            // Create a new instance for this name if not already exists
            _queueList[name] = new DigitalEventQueue(name, arrayRef);
        } else if (typeof(arrayRef) != 'undefined') {
            // If arrayRef contains any commands, push them in bulk.
            _queueList[name].pushBulk(arrayRef);
        }

        // Return the queue identified by name
        return _queueList[name];
    };

    /**
     * Get a list of all known digitaleventqueues that are initialized.
     * 
     * @returns {Object} an object containing key-value pairs of name and an instance of the DigitalEventQueue.
     */
    publicInterface.getAll = function(){
        return _queueList;
    };

    // Return the public interface of DEQFactory
    return publicInterface;
})();
