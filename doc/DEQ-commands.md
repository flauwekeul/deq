# DEQ Commands #

Commands (e.g. events pushing, listener registration) are being "pushed" into the queue.
The commands are formatted as an ordered list (_javascript array_) which always follow the following syntax:
`[ <command>, <argument 1>, <argument 2>, ...]`.

As an example: `[ 'add event', 'pageview', {'property_1' : 'value 1'}]`

_Note that there is an contrast with respect to DEQ's predecessor DCF, where there was a difference between event pushing and listener registration._  

Below you will find a list of the current accepted commands including the corresponding description of the arguments.
 
## add event ##

The command `add event` is used to push/notify the system an event happened. An optional data-object can be included in the event.

| Arg # | Name                  | Allowed type | Description           | Example    | Notes                                          |
| ----: | --------------------- | ------------ | --------------------- | ---------- | ---------------------------------------------- |
|     1 | event_name            | string       | the name of the event | 'pageview' | characters allowed: a-z A-Z 0-9 and whitespace |
|     2 | event_data [optional] | json-object  | a data object holding all properties that will be send to the listeners together with the object | { 'page_name' : 'homepage' } |  |

## add listener ##

The command `add listener` is used to register a listener with a callback function that will be called everytime the event that matches the event_name the listener listens for occurs.

| Arg # | Name                          | Allowed type  | Description                                                                                                                                           | Example                                           | Notes                                                            |
| ----: | ----------------------------- | ------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------- | ---------------------------------------------------------------- |
|     1 | listener_name                 | string        | a human readable identifier                                                                                                                           | 'console log all'                                 | characters allowed: a-z A-Z 0-9 and whitespace                   |
|     2 | callback                      | function      | a callback function that accepts two arguments, being (in order) the event_name {string} and the event_data {object}                                  | function(n,d){console.log('event:',n,'data', d);} |                                                                  |
|     3 | event_name                    | string        | a string (that will be parsed as a regex) to select the events this listener should listen for.                                                       | '.*'                                              | note that matching is done using new RegEx('/^'+event_name+'$/i) |
|     4 | include_history [optional]    | boolean       | a boolean to denote whether or not the callback funciton should be called for all the matching events that have already been pushed within page-load  | false                                             | default value true                                               |
